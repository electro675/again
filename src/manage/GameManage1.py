'''
Created on May 16, 2015

@author: Rulo
'''
import pygame
from src.api.GameManage import GameManage
from src.api.GameObject import Character, Portal

class GameManage1(GameManage):
    '''
    classdocs
    '''

    def __init__(self, params):
        '''
        Constructor
        '''
        self.fondo = self.load_image('images/fondo.png')
        self.imagenpersonajes = self.load_image('images/personaje.png')
        imagenPortal = self.load_image('images/portal.png') 
        self.screen = pygame.display.set_mode((1000, 600))
        self.reloj = pygame.time.Clock()
        pygame.display.set_caption("AGAIN")
        self.origen = [500,500]
        origenPortal = [100,100]
        self.characters = []
        self.characters.append(Character(self.imagenpersonajes, self.origen) )
        self.portal = Portal(imagenPortal, origenPortal)
        self.time = 0

    def loop(self):
        GameManage.loop(self)
        out = False    
        while out == False:            
            for event in pygame.event.get():
                for p in self.characters:
                    p.controlCollition(event, self.tiempo)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        out = True
                if event.type == pygame.QUIT:
                    out = True
            self.update()
            pygame.display.update()
            self.tiempo += 1
            self.reloj.tick(120)

    def restart(self):
        return GameManage.restart(self)


    def update(self):
        GameManage.update(self)
        self.screen.blit(self.fondo,(0,0))
        self.portal.update(self.ventana)
        for p in self.personajes:
            p.update(self.screen, self.time)


    def load_image(self, filename, transparent=False):
        return GameManage.load_image(self, filename, transparent)

        