'''
Created on May 16, 2015

@author: Rulo
'''
from src.api.GameObject import Character, GameObject


class Character1(Character):
    '''
    classdocs
    '''
    

    def __init__(self):
        '''
        Constructor
        '''

    def control(self, event):
        return Character.control(self, event)


    def clone(self):
        return Character.clone(self)


    def restart(self):
        return Character.restart(self)


    def change(self):
        return Character.change(self)


    def move(self):
        return Character.move(self)


    def saveMovement(self):
        return Character.saveMovement(self)


    def controlCollition(self, gameObject):
        return GameObject.controlCollition(self, gameObject)


    def update(self, screen):
        return GameObject.update(self, screen)
        self.rect.move_ip(self.movimiento)
        screen.blit(self.imagen, self.rect)


    def verifyCollision(self, gameObject):
        return GameObject.verifyCollision(self, gameObject)
