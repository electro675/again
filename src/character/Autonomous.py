'''
Created on May 16, 2015

@author: Rulo
'''
from src.api.State import StateCharacter, StateObject

class Autonomous(StateCharacter):
    '''
    classdocs
    '''
    

    def __init__(self, params):
        '''
        Constructor
        '''

    def handleUpdate(self, GameObject):
        return StateObject.handleUpdate(self, GameObject)


    def handleControl(self, Character):
        return StateCharacter.handleControl(self, Character)

        