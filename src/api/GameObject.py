'''
Created on May 5, 2015

@author: Rulo
'''
import pygame

class GameObject(pygame.sprite.Sprite):
    '''
    classdocs
    '''
    def __init__(self, image, position):
        '''
        Constructor
        '''
        pygame.sprite.Sprite.__init__(self)
        self.imagen = image
        self.rect=self.imagen.get_rect()
        self.rect.center = position
        self.origen = position
    
    def controlCollition(self, gameObject):
        pass
        
    def update(self, screen):
        pass
        
    def verifyCollision(self, gameObject):
        pass

class Character(GameObject):
        
    def control(self, event):
        pass
    
    def clone(self):
        pass
    
    def restart(self):
        pass
    
    def change(self):
        pass
    
    def move(self):
        pass
    
    def saveMovement(self):
        pass
    
class Portal(GameObject):
    
    def controlCollition(self, GameManage):
        pass

class Section(GameObject):
    
    def perform(self):
        pass
    
    def change(self):
        pass