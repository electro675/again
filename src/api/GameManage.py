'''
Created on May 5, 2015

@author: Rulo
'''
import pygame

class GameManage(object):
    '''
    classdocs
    '''

    def __init__(self):
        pass
       
    def loop(self):
        pass
        
    def restart(self):
        pass
    
    def update(self):
        pass
    
    def load_image(self, filename, transparent=False):
        try: image = pygame.image.load(filename)
        except (pygame.error.message):
            raise (SystemExit.message)
        if transparent:
            color = image.get_at((0,0))
            image.set_colorkey(color)
        return image