'''
Created on May 16, 2015

@author: Rulo
'''
from api.GameObject import GameObject, Section, Character

class StateObject(object):
    '''
    classdocs
    '''
    def handleUpdate(self,GameObject):
        pass

    def __init__(self, params):
        '''
        Constructor
        '''

class StateSection(StateObject):
    
    def handlePerform(self, Section):
        pass

class StateCharacter(StateObject):
    
    def handleControl(self, Character):